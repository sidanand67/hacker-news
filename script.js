let hackerNews = angular.
                    module("hackerNews", []); 

const options = {
    method: "GET",
    url: "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty",
};

hackerNews.controller("getData", function ($scope, $http) {
    $http(options)
        .then((response) => {
            return response.data;
        })
        .then((data) => {
            $scope.data = [];
            $scope.start = 0; 
            data.map((eachData) => {
                $http({
                    ...options,
                    url: `https://hacker-news.firebaseio.com/v0/item/${eachData}.json?print=pretty`,
                }).then((response) => {
                    $scope.data.push(response.data);
                });
            });
        })
        .catch((err) => console.error(err));

    $scope.showTime = (unixTime) => {
        let currentUnixTime = Math.floor(Date.now() / 1000); 
        let timeDiff = currentUnixTime - unixTime;
        let dayDiff = Math.floor(timeDiff / 86400); 
        let hourDiff = Math.floor(timeDiff / 3600); 
        let minuteDiff = Math.floor(timeDiff / 60); 

        if (dayDiff > 0) {
            return dayDiff > 1 ? dayDiff + " days ago" : dayDiff + " day ago";  
        }
        else if(hourDiff > 0) {
            return hourDiff > 1 ? hourDiff + " hours ago" : hourDiff + " hour ago"; 
        }
        else {
            return minuteDiff > 1 ? minuteDiff + " minutes ago" : minuteDiff + " minute ago"; 
        }
    }

    $scope.next = () => {
        if ($scope.start < $scope.data.length - 30){
            $scope.start += 30; 
        }
    }

    $scope.prev = () => {
        if ($scope.start != 0) {
            $scope.start -= 30;
        }
    };
});

